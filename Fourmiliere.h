#include <iostream>
#include <vector>

#ifndef _FOURMI_
#define _FOURMI_
#include "Fourmi.h"
#endif

class Fourmiliere {
    private:
        /** La quantité de la nourriture disponible dans la fourmilière */
        int qte_nourriture,
        /** Le nombre de fourmis de la fourmilière */
            population,
        /** Nombre maximal de fourmis dans la fourmilière */
            taillePopulationMax,
        /** Quantité maximale de nourriture que la fourmilière peut contenir */
            qteNourritureMax;
        /** Les fourmis de la fourmilière */
        std::vector<Fourmi> fourmis;

    public:
        /** Forme canonique */

        /** Constructeur par défaut */
        Fourmiliere();
        /** Constructeur par recopie */
        Fourmiliere(const Fourmiliere& fourmiliere);
        /** Surcharge de l'opérateur d'affectation */
        Fourmiliere& operator=(Fourmiliere *fourmiliere);
        /** Destructeur */
        ~Fourmiliere();

        /** Getters Setters */
        int getTaillePopulationMax();
        int getQteNourritureMax();
        int getQteNourriture();
        int getPopulation();
        std::vector<Fourmi> getFourmis() const;
        Fourmi getFourmisNumber(int fourmisNumber);

        void setQteNourriture(int qteNourriture);
        void setPopulation(int population);
        void setFourmis(std::vector<Fourmi> fourmis);
        void insertFourmi(Fourmi fourmi);
        void deleteFourmi(int place);
        void replaceFourmi(int place, Fourmi newFourmi);

        /** Methodes */
        /** Permet de savoir si la population de la fourmilière est pleine */
        bool isPopulationFull();
        /** Permet de savoir si le stock de nourriture de la fourmilière est pleine */
        bool isNourritureFull();
        /** Ajoute des fourmis dans la fourmilière */
        void incrementPopulation(int nombreToAdd);
        /** Gestion d'une journee de la fourmiliere */
        void newDay();
        /**
         * Fonction triant les fourmis de la fourmilière.
         * Les fourmis sont triées comme suit:
         * 1. Les premières fourmis du vecteur sont les guerrières
         * 2. Les premières guerrières du vecteur sont celles qui sont à l'extérieur de la fourmilière
         * 3. Les fourmis guerrières à l'extérieur de la fourmilières sont triées comme suit:
         *      - Par abscisse croissante
         *      - Par ordonnée croissante en cas d'abscisse égale
         */
        std::vector<Fourmi> trierFourmis(void);

    /** Surcharge de l'opérateur d'affichage */
    friend std::ostream& operator<<(std::ostream&, const Fourmiliere&);
};