#include <iterator>

#include "Terrain.h"

Terrain::Terrain(): LARGEUR(50),
           LONGUEUR(100),
           NOMBRE_OBSTACLE(100),
           NOMBRE_SOURCE_NOURRITURE(100),
           QUANTITE_NOURRRITURE_MAX_SUR_SOURCE(100),
           TAUX_EVAPORATION_PHEROMONE(0.1) {

    // Génération du tableau à 2 dimensions contenant les cases du terrain
    cases = new Case* [ this->LARGEUR ];
    for (int i=0; i < this->LARGEUR; i++)
        cases[i] = new Case[ this->LONGUEUR ];

    // Variables pour la génération des obstacles et sources de nourritures
    int abscisse,
        ordonnee;
    bool caseLibre;

    // Génération des obstacles
    for (int counter = 0 ; counter < this->NOMBRE_OBSTACLE ; counter++) {
        caseLibre = false;

        while (!caseLibre) {
            abscisse = rand() % this->LARGEUR;
            ordonnee = rand() % this->LONGUEUR;

            caseLibre = this->cases[abscisse][ordonnee].getEtat() == 0 &&
                    (abscisse != this->LARGEUR / 2 || ordonnee != this->LONGUEUR / 2);
        }

        this->cases[abscisse][ordonnee].setEtat(-1);
    }

    // Génération des sources de nourritures
    for (int counter = 0 ; counter < this->NOMBRE_OBSTACLE ; counter++) {
        caseLibre = false;

        // Tire une nouvelle case tant que les cases déjà tirée sont prises
        while (!caseLibre) {
            abscisse = rand() % this->LARGEUR;
            ordonnee = rand() % this->LONGUEUR;

            caseLibre = this->cases[abscisse][ordonnee].getEtat() == 0 &&
                        (abscisse != this->LARGEUR / 2 || ordonnee != this->LONGUEUR / 2);
        }

        // Affectation d'une quantité de nourriture aléatoire entre 1 et QUANTITE_NOURRITURE_MAX.
        this->cases[abscisse][ordonnee].setEtat(rand() % this->QUANTITE_NOURRRITURE_MAX_SUR_SOURCE + 1);
    }

    fourmiliere = new Fourmiliere();
};

Terrain::~Terrain() {}

Case** Terrain::getCases() const {
    return this->cases;
}

void Terrain::setCases(Case** &cases) {
    this->cases = cases;
}

Fourmiliere Terrain::getFourmiliere() const {
    Fourmiliere tmp = this->fourmiliere;
    tmp.setFourmis(this->fourmiliere.getFourmis());
    return tmp;
}

void Terrain::setFourmiliere(Fourmiliere &fourmiliere) {
    this->fourmiliere = fourmiliere;
}

std::ostream& operator<<(std::ostream &stream, const Terrain &toPrint) {
    std::cout << "Terrain Taille(" << toPrint.LARGEUR << ";" << toPrint.LONGUEUR << ")" << std::endl;

    std::cout << "\tCases : [\n";
    Case** cases = toPrint.getCases();

    for (int ligne = 0; ligne < toPrint.LARGEUR; ligne++) {
        for (int colonne = 0 ; colonne < toPrint.LONGUEUR ; colonne++) {
            std::cout << "\t\t" << cases[ligne][colonne] << ',' << std::endl;
        }
    }
    std::cout << "\n\t]";

    std::cout << std::endl << "\t" << toPrint.getFourmiliere();

    return stream;
}

void Terrain::display() const {
    // Parcours du tableau lignes par lignes
    for (int ligne = 0; ligne < this->LARGEUR; ligne++) {
        for (int colonne = 0; colonne < this->LONGUEUR; colonne++) {

            // Teste pour l'affichage de la fourmilière
            if (ligne == this->LARGEUR / 2 && colonne == this->LONGUEUR / 2) {
                std::cout << 'F';
            }
            else {
                // Affiche le caractère correspondant au type de case
                switch (this->cases[ligne][colonne].getEtat()) {
                    case 0:
                        std::cout << '_';
                        break;
                    case -1:
                        std::cout << 'X';
                        break;
                    default:
                        std::cout << 'o';
                        break;
                }
            }
        }
        std::cout << std::endl;
    }
}

void Terrain::deplacementFourmi() {
    srand((int) time(NULL));

    for (Fourmi fourmi : this->fourmiliere.getFourmis()) {
        if (fourmi.getRole() == RoleFourmis::GUERRIERE) { //Seule les guerrières se déplacent
            if (!fourmi.estDansFourmiliere() || !this->fourmiliere.isNourritureFull()) { //Se déplacent seulement s'il manque de nourriture ou qu'elles sont en dehors
                int xFourmi = fourmi.getAbscisseFourmi(), yFourmi = fourmi.getOrdonneeFourmi();
                int xFinal, yFinal;
                bool isvalidCase = false;
                if ((fourmi.getNourritureTransportable() - fourmi.getNourritureTransportee() > 0) && fourmi.getFaim() >= fourmi.SEUIL_FAIM) { //Peut encore recuperer de la nourriture --> deplacement pseudo-aleatoire et n'as pas faim
                    while (!isvalidCase) {
                        int valDeplacement = rand() % 4;
                        switch (valDeplacement) {
                            case 0: //Déplacement vers le haut
                                if (cases[xFourmi][yFourmi - 1].getEtat() >= 0) {
                                    xFinal = xFourmi;
                                    yFinal = yFourmi - 1;
                                    isvalidCase = true;
                                }
                                break;
                            case 1: //Déplacement vers la droite
                                if (cases[xFourmi + 1][yFourmi].getEtat() >= 0) {
                                    xFinal = xFourmi + 1;
                                    yFinal = yFourmi;
                                    isvalidCase = true;
                                }
                                break;
                            case 2: //Déplacement vers le bas
                                if (cases[xFourmi][yFourmi + 1].getEtat() >= 0) {
                                    xFinal = xFourmi;
                                    yFinal = yFourmi + 1;
                                    isvalidCase = true;
                                }
                                break;
                            case 3: //Déplacement vers la gauche
                                if (cases[xFourmi - 1][yFourmi].getEtat() >= 0) {
                                    xFinal = xFourmi - 1;
                                    yFinal = yFourmi;
                                    isvalidCase = true;
                                }
                                break;
                            default:
                                break;
                        } //switch
                    } //while
                } else { //Ne peut plus recuperer de la nourriture --> deplacement vers la fourmiliere
                    int deltaX, deltaY;
                    deltaX = xFourmi - LARGEUR/2;
                    deltaY = yFourmi - LONGUEUR/2;

                    if (abs(deltaX) > abs(deltaY)) { //Plus eloignee en X qu'en Y
                        if (deltaX > 0)
                            xFinal = xFourmi - 1;
                        else
                            xFinal = xFourmi + 1;

                        yFinal = yFourmi;
                    } else { //Plus eloignee en Y qu'en X
                        xFinal = xFourmi;

                        if (deltaY > 0)
                            yFinal = yFourmi - 1;
                        else
                            yFinal = yFourmi + 1;
                    }

                }

                if (xFinal == LARGEUR/2 && yFinal == LONGUEUR/2) {
                    fourmi.setDansFourmiliere(true);
                    fourmi.setAbscisseFourmi(-1);
                    fourmi.setOrdonneeFourmi(-1);

                    int qteNourritureFourmiliere = fourmiliere.getQteNourriture();

                    //Depose la nourriture
                    fourmiliere.setQteNourriture(qteNourritureFourmiliere + fourmi.getNourritureTransportee());

                    //Mange, les guerrieres mangent uniquement dans la fourmiliere
                    if (qteNourritureFourmiliere > 0) {
                        fourmi.setFaim(100);
                        fourmiliere.setQteNourriture(qteNourritureFourmiliere - 1);
                    }
                } else {
                    fourmi.setDansFourmiliere(false);
                    fourmi.setAbscisseFourmi(xFinal);
                    fourmi.setOrdonneeFourmi(yFinal);
                    int qteNourriture = cases[xFinal][yFinal].getEtat();
                    if (qteNourriture > 0) { //Présence de nourriture
                        int nourriturePrenable = fourmi.getNourritureTransportable() - fourmi.getNourritureTransportee();
                        if (nourriturePrenable >= qteNourriture) { //Elle peut prendre toute la nourriture de la case
                            fourmi.setnourritureTransportee(fourmi.getNourritureTransportee() + qteNourriture);
                            cases[xFinal][yFinal].setEtat(0);
                        } else { //Elle ne peut pas prendre toute la nourriture
                            fourmi.setnourritureTransportee(fourmi.getNourritureTransportee() + nourriturePrenable);
                            cases[xFinal][yFinal].setEtat(qteNourriture - nourriturePrenable);
                        }
                    }
                }

            } //if peut se déplacer
        } //if c'est une guerrière
    } //for
}