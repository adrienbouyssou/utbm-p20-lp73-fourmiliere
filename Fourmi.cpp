#include "Fourmi.h"

Fourmi::Fourmi()
{
    probabiliteEvolution = PROBABILITE_EVOLUTION_DEFAUT;
    probabiliteDeces = PROBABILITE_DECES_DEFAUT;
    futureReine = false;
    faim = 100;
    age = 0;
    role = RoleFourmis::OEUF;
    nombreToursAvantProchainePonte = PERIODE_PONTE;
    abscisseFourmi = -1;
    ordonneeFourmi = -1;
}

Fourmi::Fourmi(const Fourmi& fourmi) {
    this->probabiliteEvolution = fourmi.probabiliteEvolution;
    this->probabiliteDeces = fourmi.probabiliteDeces;
    this->futureReine = fourmi.futureReine;
    this->faim = fourmi.faim;
    this->age = fourmi.age;
    this->role = fourmi.role;
    this->nombreToursAvantProchainePonte = fourmi.nombreToursAvantProchainePonte;
    this->abscisseFourmi = fourmi.abscisseFourmi;
    this->ordonneeFourmi = fourmi.ordonneeFourmi;
}

Fourmi& Fourmi::operator=(const Fourmi& fourmi) {
    this->probabiliteEvolution = fourmi.probabiliteEvolution;
    this->probabiliteDeces = fourmi.probabiliteDeces;
    this->futureReine = fourmi.futureReine;
    this->faim = fourmi.faim;
    this->age = fourmi.age;
    this->abscisseFourmi = fourmi.abscisseFourmi;
    this->ordonneeFourmi = fourmi.ordonneeFourmi;

    return (*this);
}

Fourmi::~Fourmi() {
    // Empty body
}


Fourmi::Fourmi(RoleFourmis role) {
    probabiliteEvolution = PROBABILITE_EVOLUTION_DEFAUT;
    probabiliteDeces = PROBABILITE_DECES_DEFAUT;
    futureReine = false;
    faim = 100;
    age = 0;
    this->role = role;
    nombreToursAvantProchainePonte = PERIODE_PONTE;
    ordonneeFourmi = -1;
}

float Fourmi::getProbabiliteEvolution() {
    return this->probabiliteEvolution;
}

float Fourmi::getProbabiliteDeces() {
    return this->probabiliteDeces;
}

bool Fourmi::estFutureReine() {
    return this->futureReine;
}

bool Fourmi::estDansFourmiliere() {
    return abscisseFourmi == -1 && ordonneeFourmi == -1;
}

int Fourmi::getFaim() {
    return this->faim;
}

int Fourmi::getAge() {
    return this->age;
}

int Fourmi::getNourritureTransportable() {
    return this->nourritureTransportable;
}

int Fourmi::getNourritureTransportee() {
    return this->nourritureTansportee;
}

int Fourmi::getNombreToursAvantProchainePonte() {
    return this->nombreToursAvantProchainePonte;
}

RoleFourmis Fourmi::getRole() {
    return this->role;
}

int Fourmi::getAbscisseFourmi() {
    return this->abscisseFourmi;
}

int Fourmi::getOrdonneeFourmi() {
    return this->ordonneeFourmi;
}

void Fourmi::setProbabiliteEvolution(float probabiliteEvolution) {
    this->probabiliteEvolution = probabiliteEvolution;
}

void Fourmi::setProbabiliteDeces(float probabiliteDeces) {
    this->probabiliteDeces = probabiliteDeces;
}

void Fourmi::setFutureReine(bool futureReine) {
    this->futureReine = futureReine;
}

void Fourmi::setDansFourmiliere(bool inFourmiliere) {
    this->inFourmiliere = inFourmiliere;
}

void Fourmi::setFaim(int faim) {
    this->faim = faim;
}

void Fourmi::setAge(int age) {
    this->age = age;
}

void Fourmi::setNourritureTransportable(int nourritureTransportable) {
    this->nourritureTransportable = nourritureTransportable;
}

void Fourmi::setnourritureTransportee(int nourritureTransportee) {
    this->nourritureTansportee = nourritureTransportee;
}

void Fourmi::setNombreToursAvantProchainePonte(int nombreToursAvantProchainePonte)
{
    this->nombreToursAvantProchainePonte = nombreToursAvantProchainePonte;
}

void Fourmi::setRole(RoleFourmis role) {
    this->role = role;
}

void Fourmi::setAbscisseFourmi(int abscisseFourmi) {
    this->abscisseFourmi = abscisseFourmi ;
}

void Fourmi::setOrdonneeFourmi(int ordonneeFourmi) {
    this->ordonneeFourmi = ordonneeFourmi ;
}

/** Methodes */

int Fourmi::manger() {
    if (this->faim <= SEUIL_FAIM) {
        this->faim = MAX_FAIM;
        
        return (this->role == RoleFourmis::REINE ? 2 : 1);
    }

    return 0;
}

int Fourmi::pondre() {
    nombreToursAvantProchainePonte--;

    if (nombreToursAvantProchainePonte > 0)
        return  0;

    nombreToursAvantProchainePonte = PERIODE_PONTE;
    return 10;
}

void Fourmi::seDeplacer() {

}

void Fourmi::prendreNourriture() {

}

int Fourmi::dropNourriture() {
    return 0;
}

int Fourmi::evolution() {
    srand((int) time(NULL));

    bool willEvolve = (rand() % 100) < this->probabiliteEvolution*100;
    bool die = (rand() % 100) < this->probabiliteDeces*100;

    if (die) {
        return -1;
    }

    if (this->role == RoleFourmis::REINE) {
        if (this->age >= 3600) {
            this->probabiliteDeces += 1/140;
        }
    } else {
        if (this->age < 650) {
            this->probabiliteDeces += 0.05/650;
        } else if (this->age >= 650 && this->age <=740) {
            this->probabiliteDeces += (this->age/740 - 0.87)/9;
        } else {
            this->probabiliteDeces += 0.25/110;
        }
    }
    

    if (willEvolve) {
        switch (this->role) {
            case RoleFourmis::OEUF:
                this->role = RoleFourmis::LARVE;
                break;

            case RoleFourmis::LARVE:
                this->role = RoleFourmis::OUVRIERE;
                break;

            case RoleFourmis::OUVRIERE:             
                this->role = (this->estFutureReine() ? RoleFourmis::REINE : RoleFourmis::GUERRIERE);
                break;
        
            default:
                break;
        }

        this->probabiliteEvolution = 0;
        this->dureeDepuisChgmntRole = 0;

        return 1;
    } else {
        dureeDepuisChgmntRole++;

        switch (this->role) {
            case RoleFourmis::OEUF:
            case RoleFourmis::LARVE:
                if (this->dureeDepuisChgmntRole >= 29) {
                    this->probabiliteEvolution += 0.1;
                }
                
                break;

            case RoleFourmis::OUVRIERE:             
                if (this->dureeDepuisChgmntRole >= 550) {
                    this->probabiliteEvolution += 0.01;
                }
                
                break;
        
            default:
                break;
        }

        return 0;
    }
}