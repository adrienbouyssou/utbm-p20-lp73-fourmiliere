#ifndef _CASE_
#define _CASE_
#include "Case.h"
#endif

#ifndef _FOURMILIERE_
#define _FOURMILIERE_
#include "Fourmiliere.h"
#endif

#ifndef _FOURMI_
#define _FOURMI_
#include "fourmi.h"
#endif

#include <vector>
#include <cstdlib>
#include <time.h>
#include <cmath>

class Terrain {
public:
    const int
        /** Largeur du terrain */
        LARGEUR,
        /** Longueur du terrain */
        LONGUEUR,
        /** Nombre d'obstacles sur le terrain */
        NOMBRE_OBSTACLE,
        /** Nombre de source de nourriture sur le terrain */
        NOMBRE_SOURCE_NOURRITURE,
        /** Quantité maximale de nourriture sur une source */
        QUANTITE_NOURRRITURE_MAX_SUR_SOURCE;
        /** Taux d'évaporation des phéromones */
    const float TAUX_EVAPORATION_PHEROMONE;

private:
    /** Tableau à 2 dimensions des cases du terrain */
    Case** cases;
    /** Fourmilière sur le terrain */
    Fourmiliere fourmiliere;

public:
    /** Forme canonique */

    /** Constructeur par défaut */
    Terrain();
    /** Constructeur par recopie */
    Terrain(const Terrain &base): LARGEUR(base.LARGEUR), LONGUEUR(base.LONGUEUR),
        NOMBRE_OBSTACLE(base.NOMBRE_OBSTACLE), NOMBRE_SOURCE_NOURRITURE(base.NOMBRE_SOURCE_NOURRITURE),
        QUANTITE_NOURRRITURE_MAX_SUR_SOURCE(base.QUANTITE_NOURRRITURE_MAX_SUR_SOURCE),
        TAUX_EVAPORATION_PHEROMONE(base.TAUX_EVAPORATION_PHEROMONE),
        cases(base.cases), fourmiliere(Fourmiliere(base.fourmiliere)) {};
    /** Destructeur */
    ~Terrain();

    /** Getters Setters */
public:
    Case** getCases() const;
    void setCases(Case**&);
    Fourmiliere getFourmiliere() const;
    void setFourmiliere(Fourmiliere&);
    void display() const;
    void deplacementFourmi();

    /** Surchage de l'opérateur d'affichage */
    friend std::ostream& operator<<(std::ostream&, const Terrain&);
};