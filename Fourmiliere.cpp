#include "Fourmiliere.h"
#include <iostream>
Fourmiliere::Fourmiliere() {
    taillePopulationMax = 100;
    qteNourritureMax = 100;
    qte_nourriture = 100;
    population = 100;

    std::vector<Fourmi> mesFourmisTest;

    for (int counter = 0 ; counter < population ; counter++) {
        if (counter < population / 2) {
            mesFourmisTest.push_back(Fourmi(RoleFourmis::OEUF));
        }
        else {
            mesFourmisTest.push_back(Fourmi(RoleFourmis::GUERRIERE));
        }
    }

    fourmis = mesFourmisTest;
}

Fourmiliere::Fourmiliere(const Fourmiliere& fourmiliere) {
    this->taillePopulationMax = fourmiliere.taillePopulationMax;
    this->qteNourritureMax = fourmiliere.qteNourritureMax;
    this->qte_nourriture = fourmiliere.qte_nourriture;
    this->population = fourmiliere.population;
}

Fourmiliere& Fourmiliere::operator=(Fourmiliere *fourmiliere) {
    this->taillePopulationMax = fourmiliere->taillePopulationMax;
    this->qteNourritureMax = fourmiliere->qteNourritureMax;
    this->qte_nourriture = fourmiliere->qte_nourriture;
    this->population = fourmiliere->population;
    this->fourmis = fourmiliere->fourmis;

    return(*this);
}

Fourmiliere::~Fourmiliere() {}

/** Getter/Setters */
int Fourmiliere::getQteNourriture() {
    return this->qte_nourriture;
}

int Fourmiliere::getPopulation() {
    return this->population;
}

std::vector<Fourmi> Fourmiliere::getFourmis() const {
    return this->fourmis;
}

Fourmi Fourmiliere::getFourmisNumber(int fourmisNumber) {
    return this->fourmis[fourmisNumber];
}

void Fourmiliere::setQteNourriture(int qteNourriture) {
    if (qteNourriture > this->qteNourritureMax)
        this->qte_nourriture = this->qteNourritureMax;
    else
        this->qte_nourriture = qteNourriture;
}

void Fourmiliere::setPopulation(int populationActuelle) {
    this->population = populationActuelle;
}

void Fourmiliere::setFourmis(std::vector<Fourmi> fourmis) {
    this->fourmis = fourmis;
}

void Fourmiliere::insertFourmi(Fourmi fourmi) {
    this->fourmis.push_back(fourmi);
}

void Fourmiliere::deleteFourmi(int place) {
    this->fourmis.erase(fourmis.begin() + place);
}

void Fourmiliere::replaceFourmi(int place, Fourmi newFourmi) {
    this->fourmis.at(place) = newFourmi;
}

/** Méthodes */
bool Fourmiliere::isPopulationFull() {
    return population >= taillePopulationMax;
}

bool Fourmiliere::isNourritureFull() {
    return qte_nourriture = qteNourritureMax;
}

void Fourmiliere::incrementPopulation(int nombreToAdd) {
    int nvlFourmi = 0;

    for (int i = 0; i < nombreToAdd; i++) {
        Fourmi nouvelleFourmi = Fourmi(RoleFourmis::OEUF);
        if (!this->isPopulationFull() || nouvelleFourmi.estFutureReine()) {
            fourmis.push_back(nouvelleFourmi);
            nvlFourmi++;
        }
    }

    this->incrementPopulation(nvlFourmi);
}

void Fourmiliere::newDay() {
    for (Fourmi fourmi : fourmis) {
        if (fourmi.getRole() == RoleFourmis::REINE) {
            if (qte_nourriture >= 2)
                this->qte_nourriture -= fourmi.manger();

            this->incrementPopulation(fourmi.pondre());
            
        } else {
            if (qte_nourriture >= 1 && fourmi.getRole() != RoleFourmis::GUERRIERE) //Guerriere gerer avec le deplacement car mange que dans la fourmiliere
                this->qte_nourriture -= fourmi.manger();

            fourmi.evolution();
        }
    }
}

std::vector<Fourmi> Fourmiliere::trierFourmis() {
    int fourmisNonGuerrieresInterieur = 0,
        counter = 0;
    for (std::vector<Fourmi>::iterator it = fourmis.begin() ;
            it != fourmis.end() && counter + fourmisNonGuerrieresInterieur < population ;
            it = fourmis.begin() + counter) {

        // Tri des fourmis non-guerrière
        if (it->getRole() != RoleFourmis::GUERRIERE) {
            fourmis.insert(fourmis.end() - 1, fourmis[counter]);
            it = fourmis.begin() + counter;
            fourmis.erase(fourmis.begin());
        }

        // Tri des guerrières dans la fourmillière
        else if (it->getRole() == RoleFourmis::GUERRIERE && it->estDansFourmiliere()) {
            fourmis.insert(fourmis.end() - 1 - fourmisNonGuerrieresInterieur, fourmis[counter]);
        }

        // Tri des guerrières hors de la fourmilière
        else {
            // Recherche de la place de la fourmi dans la partie du tableau des guerrières qui sont à l'extérieur
            int counterForInsert;
            for (counterForInsert = 0 ;
                    counterForInsert < counter && (
                        fourmis[counterForInsert].getAbscisseFourmi() < fourmis[counter].getAbscisseFourmi() ||
                            (fourmis[counterForInsert].getAbscisseFourmi() == fourmis[counter].getAbscisseFourmi() &&
                            fourmis[counterForInsert].getOrdonneeFourmi() < fourmis[counter].getOrdonneeFourmi())) ;
                    counterForInsert++);
                // Empty body

            fourmis.insert(fourmis.begin() + counterForInsert, fourmis[counter]);
            counter++;
        }
        counter++;
    }

    return fourmis;
}

std::ostream& operator<<(std::ostream &stream, const Fourmiliere &toPrint) {
    std::cout << "Fourmiliere(population : " << toPrint.population << "/" << toPrint.taillePopulationMax <<
        " - nourriture : " << toPrint.qte_nourriture << "/" << toPrint.qteNourritureMax << ")";
    return stream;
}