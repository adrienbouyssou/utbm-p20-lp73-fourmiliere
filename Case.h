class Case {
    private:
        /**
         * Etat de la case.
         *
         * Peut prendre trois types de valeurs:
         *
         * - 0 : Indique que la case est classique.
         * - -1 : Indique que la case est un obstacle.
         * - Nombre positif : Indique que la case est une source de nourriture.
         *                      Dans ce cas la valeur de etat indique la quantité de nourriture restante.
         */
        int etat;

    public:
        /** Forme canonique */

        /**
         * Constructeur par défaut. Créé une case classique.
         */
        Case (): etat(0) {};

        /**
         * Constructeur par recopie. Créé une case qui est une copie de la case en paramètre.
         *
         * @param base La case à copier.
         */
        Case (const Case& base):
            etat(base.etat){};

        /**
         * Surcharge de l'opérateur d'affectation.
         * Affecte la valeur de la case passéée en paramètre à la case courrante.
         *
         * @param toAffect La case à affecter à la case courrante.
         * @return Une référence de la case après l'affectation.
         */
        Case& operator=(const Case& toAffect);

        /** Desctructeur de la case */
        ~Case();

        /** Constructeur.
         * Constructeur avec un état défini à affecter.
         */
        Case(const int etat): etat(etat) {};

        /** Getters Setters */
        int getEtat() const;
        void setEtat(const int);

        /**
         * Surcharge de l'opérateur d'affichage.
         * @return Le flux de sortie après le traitement.
         */
        friend std::ostream& operator<<(std::ostream&, const Case&);
};