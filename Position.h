#include <iostream>

class Position {
    private:
        /** Abscisse de la position */
        int coordonneeX,
        /** Ordonnée de la position */
            coordonneeY;

    public:
        /** Forme canonique */

        /** Constructeur par défaut */
        Position(): 
            coordonneeX(0),
            coordonneeY(0) {};
        /** Constructeur par recopie */
        Position(const Position& position): 
            coordonneeX(position.coordonneeX), 
            coordonneeY(position.coordonneeY) {};
        /** Surcharge de l'opérateur d'affectation */
        Position& operator=(const Position&);
        /** Destructeur */
        ~Position();

        /** Constructeur en donnant les positions */
        Position(const int x, const int y):
            coordonneeX(x),
            coordonneeY(y) {};

        /** Getters Setters */
        int getX() const;
        int getY() const;
        void setX(int);
        void setY(int);

        /** Surcharge de l'opérateur d'affichage */
        friend std::ostream& operator<<(std::ostream&, const Position&);
};