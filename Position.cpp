#include <iostream>

#include "Position.h"

Position& Position::operator=(const Position& position) {
    this->coordonneeX = position.coordonneeX;
    this->coordonneeY = position.coordonneeY;

    return (*this);
}

Position::~Position() {}

std::ostream& operator<<(std::ostream &stream, const Position& position) {
    std::cout << "Position(" << position.coordonneeX << ";" << position.coordonneeY << ")";
    return stream;
}

int Position::getX() const {
    return this->coordonneeX;
}

int Position::getY() const {
    return this->coordonneeY;
}

void Position::setX(int x) {
    this->coordonneeX = x;
}

void Position::setY(int y) {
    this->coordonneeY = y;
}