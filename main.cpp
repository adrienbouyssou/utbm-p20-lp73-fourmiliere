#ifndef _FOURMI_
#define _FOURMI_
#endif

#ifndef _FOURMILIERE_
#define _FOURMILIERE_
#endif

#ifndef _POSITION_
#define _POSITION_
#endif

#ifndef _CASE_
#define _CASE_
#endif

#ifndef _TERRAIN_
#define _TERRAIN_
#endif

#include "Fourmi.h"
#include "Fourmiliere.h"
#include "Position.h"
#include "Case.h"
#include "Terrain.h"

#include <time.h>

int main() {
    // Permet de rendre plus aléatoire les tirages. Améliore la génération de terrain différents.
    srand((int) time(NULL));

    // Génération et premier affichage du terrain
    Terrain terrain;

    /** Init des fourmis */
    std::vector<Fourmi> nosFourmis = terrain.getFourmiliere().getFourmis();

    nosFourmis.push_back(Fourmi(RoleFourmis::REINE));

    for (int i = 1; i < 100; i++) {
        switch (i % 4) {
            case 1:
                nosFourmis.push_back(Fourmi(RoleFourmis::LARVE));
                break;
            case 2:
                nosFourmis.push_back(Fourmi(RoleFourmis::OUVRIERE));
                break;
            case 3:
                nosFourmis.push_back(Fourmi(RoleFourmis::GUERRIERE));
                break;
            default:
                nosFourmis.push_back(Fourmi(RoleFourmis::OEUF));
                break;
        }
    }

    terrain.getFourmiliere().setFourmis(nosFourmis);

    bool isRunning = true;
    std::string input;

    while (isRunning) {
        std::cout << "\n Enter exit pour sortir de la simulation, n'importe quel autre caracteres permet de creer un nouveau round :  ";
        std::cin >> input;
        std::cout << "\n" << std::endl;
        if (input == "exit") {
            isRunning = false;
        }

        /** Gestion d'un jour */
        terrain.getFourmiliere().newDay();
        terrain.deplacementFourmi();
        terrain.display();
    }

    /*std::cout << "Entrez un caractère pour fermer la fenêtre : ";
    getchar();*/

    /*int nbGuerrieresDehors = 0;
    std::vector<Fourmi> fourmis = terrain.getFourmiliere().getFourmis();
    for (std::vector<Fourmi>::iterator it = fourmis.begin() ;
            it != terrain.getFourmiliere().getFourmis().end() &&
            nbGuerrieresDehors < 10 ;
            it++) {
        if (it->getRole() == RoleFourmis::GUERRIERE) {
            it->setAbscisseFourmi(rand() % 50);
            it->setOrdonneeFourmi(rand() % 100);
            nbGuerrieresDehors++;
        }
    }

    terrain.getFourmiliere().setFourmis(fourmis);

    terrain.getFourmiliere().setFourmis(terrain.getFourmiliere().trierFourmis());

    std::cout << " #################################################################################################" << std::endl <<
        " #################################################################################################" << std::endl;

    for (std::vector<Fourmi>::iterator it = terrain.getFourmiliere().getFourmis().begin() ;
         it != terrain.getFourmiliere().getFourmis().end() ;
         it++) {
        switch (it->getRole()) {
            case RoleFourmis::GUERRIERE : std::cout << it->getAbscisseFourmi() << " ; " << it->getOrdonneeFourmi() << std::endl; break;
            case RoleFourmis::OEUF : std::cout << "oeuf" << std::endl; break;
            default : std::cout << "autre" << std::endl; break;
        }
    }*/

    return 0;
}