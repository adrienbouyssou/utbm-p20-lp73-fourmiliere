#include <iostream>
#include <cstdlib>
#include <time.h>

#ifndef _POSITION_
#define _POSITION_
#include "Position.h"
#endif

enum class RoleFourmis : short {REINE, GUERRIERE, OUVRIERE, LARVE, OEUF};

class Fourmi {
    /* Constantes */
    public:
        /** Consommation de nourriture d'une fourmi par tour */
        const int CONSOMMATION_NOURRITURE = 1;
        /** Consommation de la reine par tour */
        const int CONSOMMATION_NOURRITURE_REINE = 2;
        /** Durée de vie moyenne d'une fourmi */
        const int DUREE_VIE_MOYENNE = 100;
        /**
         * Seuil à partir du quel la fourmi va avoir faim.
         * Elle mangera afin de rester au-dessus de ce seuil
         */
        const int SEUIL_FAIM = 25;
        /**
         * Probabilité qu'une fourmi passe au stade supérieur juste après son évolution.
         */
        const float PROBABILITE_EVOLUTION_DEFAUT = 0.00;
        /**
         * Probabilité que la fourmi décède juste après l'évolution.
         */
        const float PROBABILITE_DECES_DEFAUT = 0.05;
        /**
         * Taux supplémentaire affecté à la probabilité d'évolution de la fourmi après chaque tour.
         */
        const float TAUX_SUPPLEMENTAIRE_EVOLUTION = 0.05;
        /**
         * Taux supplémentaire affecté à la probabilité de décès de la fourmi après chaque tour.
         */
        const float TAUX_SUPPLEMENTAIRE_DECES = 0.05;
        /**
         * Niveau de faim maximal. Si la fourmi est à ce niveau, alors elle n'a pas faim.
         * Lorsqu'une fourmi mange, elle consomme de la nourriture jusqu'à atteindre ce niveau.
         */
        const int MAX_FAIM = 100;

        /** Probabilité que l'oeuf pondu soit une future reine */
        const float PROBABILITE_PONTE_REINE = 0.01;
        /** Probabilité que l'oeuf pondu soit une future reine si la fourmilière a atteind sa population maximale */
        const float PROBABILITE_PONTE_REINE_AVEC_POPULATION_MAXIMALE = 0.75;
        /** Nombre de tour avant que la reine ponde à nouveau */
        const int PERIODE_PONTE = 20;
    
    /* Attributs de la fourmi */
    private:
        /** Probabilité d'évolution de la fourmi au prochain tour */
        float probabiliteEvolution;
        /** Probabilité de décès de la fourmi au prochain tour */
        float probabiliteDeces;
        /** Indique si la fourmi est une future Reine (true) ou non (false) */
        bool futureReine;
        /** Indique si la fourmi est dans la fourmilière (true) ou non (false) */
        bool inFourmiliere;
        /** Indicateur du niveau de faim de la fourmi */
        int faim;
        /** Age de la fourmi */
        int age;
        /** Quantité de nourriture que la fourmi peut transporter au maximum */
        int nourritureTransportable;
        /** Quantité de nourriture que la fourmi transporte actuellement  */
        int nourritureTansportee;
        /** Nombre de tours restants avant la prochaine ponte */
        int nombreToursAvantProchainePonte;
        /** Rôle de la fourmi au sein de la fourmilière */
        RoleFourmis role;
        /** Durée depuis la dernière évolution */
        int dureeDepuisChgmntRole = 0;
        /** Abscisse de la fourmi sur la carte */
        int abscisseFourmi,
        /** Ordonnee de la fourmi sur la carte */
            ordonneeFourmi;
    
    public:
        /** Forme canonique */
        /** Constructeur par défaut. Complète les attributs de la fourmi avec les valeurs par défaut */
        Fourmi();
        /** Constructeur par recopie. Complète les attributs de la fourmi avec les valeurs de la fourmi donnée */
        Fourmi(const Fourmi& fourmi);
        /** Surcharge de l'opérateur d'affectation */
        Fourmi& operator=(const Fourmi& fourmi);
        /** Desctructeur de la fourmi */
        ~Fourmi();

        /** Constructeur. Affecte un rôle spécifique à la fourmi */
        Fourmi(RoleFourmis role);

        /** Getters Setters */
        float getProbabiliteEvolution();
        float getProbabiliteDeces();
        bool estFutureReine();
        bool estDansFourmiliere();
        int getFaim();
        int getAge();
        int getNourritureTransportable();
        int getNourritureTransportee();
        int getNombreToursAvantProchainePonte();
        RoleFourmis getRole();
        int getAbscisseFourmi();
        int getOrdonneeFourmi();

        void setProbabiliteEvolution(float probabiliteEvolution);
        void setProbabiliteDeces(float probabiliteDeces);
        void setFutureReine(bool futureReine);
        void setDansFourmiliere(bool inFourmiliere);
        void setFaim(int faim);
        void setAge(int age);
        void setNourritureTransportable(int nourritureTransportable);
        void setnourritureTransportee(int nourritureTransportee);
        void setNombreToursAvantProchainePonte(int nombreTourAvantProchainePonte);
        void setRole(RoleFourmis role);
        void setAbscisseFourmi(int abscisseFourmi);
        void setOrdonneeFourmi(int ordonneeFourmi);

        /** Methodes */
        /** Fait manger la fourmi. Elle tente de remplir sa jauge de faim au maximum. */
        int manger();
        /** La fourmi pond */
        int pondre();
        /** Déplacement de la fourmi. Possible uniquement pour les guerrières. */
        void seDeplacer();
        /** La fourmi se charge en nourriture à ramener à la fourmilière. Elle tente d'en prendre un maximum */
        void prendreNourriture();
        /** La fourmi dépose la nourriture qu'elle transporte dans la fourmilière */
        int dropNourriture();
        /** La fourmi regarde si elle doit évoluer ou mourrir */
        int evolution();
};