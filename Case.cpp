#include <iostream>

#include "Case.h"

Case& Case::operator=(const Case& toSet)
{
    this->etat = toSet.getEtat();

    return (*this);
}

Case::~Case() {}

int Case::getEtat() const
{
    return this->etat;
}

void Case::setEtat(const int etat)
{
    this->etat = etat;
}

std::ostream& operator<<(std::ostream &stream, const Case &toPrint) {
    std::cout << "Case { ";

    int etat = toPrint.getEtat();

    switch (etat) {
        case 0: std::cout << "Type: normal"; break;
        case -1: std::cout << "Type: obstacle"; break;
        default: std::cout << "Type: nourriture - Nourriture restante: " << etat; break;
    }

    std::cout << " }";

    return stream;
}